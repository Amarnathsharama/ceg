app.service('loginService', ['$http', function ($http) {
    var authenticate = {
      
        signIn: function (user, callback) {
            // '/api/users/' + user.email + '/login', user
            var url = 'http://13.229.90.65:8001/api/sanghi/login'
            // lurl = url + '?' +  user.userid & user.password;
            $http.get(url, {params:{"userid": user.userid , "password": user.password}})
                .success(function (result) {
                    callback(result);
                })
                .error(function (err) {
                    callback(err);
                });
        },

    };
    return {
        signInUser: authenticate.signIn,
    };

}]);
