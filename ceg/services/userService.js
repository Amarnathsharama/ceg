app.service('userService', ['$http','$q', function ($http,$q) {
    var user = {
      
        // get: function (URL,filters) {
        //     var deferred = $q.defer();
        //       if (filters && filters.length > 0) {
        //         URL = URL + '?' + filters.join("&");
        //     }
        //     $http.get(URL)
        //         .success(function (data) {
        //             deferred.resolve(data);
        //         })
        //         .error(function (data) {
        //             deferred.reject(data);
        //         });
        //     return deferred.promise;
        // },
        get: function (URL,filters) {
            var deferred = $q.defer();
              if (filters && filters.length > 0) {
                URL = URL + '?' + filters.join("&");
            }
            $http.get(URL)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        },
    };
    return {
        get: user.get,
    };

}]);
