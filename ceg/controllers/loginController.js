app.controller('loginController', ['$scope', 'userService', 'Excel', '$timeout', '$controller', '$location', '$http', '$cookies', '$rootScope', '$filter', function ($scope, userService, Excel, $timeout, $controller, $location, $http, $cookies, $rootScope, $filter) {


    $scope.loginform = true;
    // $rootScope.loginStatus="LogOff";
    var status = $cookies.get("loginnStatus");
    console.log("Status Testing", status)


    var privateMethods = {
        commonFilters: function () {
            var filters = [];
            var name = "name=" + $scope.name;
            var title = "title=" + $scope.title;
            var email = "email=" + $scope.emailid;
            var password = "password=" + $scope.password;
            var mobile = "mobile=" + $scope.mobileno;
            var company = "company=" + $scope.company;
            var address = "address=" + $scope.comapnyaddress;
            var city = "city=" + $scope.city;
            var state = "state=" + $scope.state;
            var zip = "zip=" + $scope.zipcode;


            filters.push(name);
            filters.push(email);
            filters.push(password);
            filters.push(title);
            filters.push(mobile);
            filters.push(company);
            filters.push(address);
            filters.push(city);
            filters.push(state);
            filters.push(zip);


            return filters;
        },
        notifyFilters: function () {
            var filters = [];
            var userid = "loggeduserid=" + testd;
            filters.push(userid);
            return filters;
        },
    }
    $scope.resetFunc = function () {
        $scope.name = ""
        $scope.title = ""
        $scope.emailid = ""
        $scope.password = ""
        $scope.mobileno = ""
        $scope.company = ""
        $scope.comapnyaddress = ""
        $scope.city = ""
        $scope.state = ""
        $scope.zipcode = ""
    }

    $scope.login = function () {
        console.log("$scope.uname", $scope.uname);
        console.log("scope.pwd", $scope.pwd);
        if ($scope.uname == null || $scope.uname == undefined || $scope.pwd == null || $scope.pwd == undefined) {
            alert("Please enter both Email id and Password")
        }
        if ($scope.uname != null && $scope.pwd != null) {
            // $rootScope.loginStatus="LogIn";
            $cookies.put("loginnStatus", "LogIn");
            console.log("Login Status", $rootScope.loginStatus)
            $scope.loggedin = true;
            $cookies.put("loggedValue", $scope.loggedin);
            $cookies.get('loggedValue');
            console.log("loggedValue", $cookies.get('loggedValue'));
            $location.path('/')
            window.sessionStorage.currentLoggedInUser = $cookies.get('loggedValue');
        }
    }

    $scope.signup = function () {

        var url = "http://35.154.115.230:8001/api/registration"
        var filters = privateMethods.commonFilters();
        userService.get(url, filters).then(
            function (response) {
                $scope.signupResponse = response;
                console.log("signupResponse", $scope.signupResponse)
                if ($scope.signupResponse == "inserted") {
                    alert("registered succesfully");
                    $scope.loginform = true;
                    $scope.signupform = false;
                }
            })

    }
}]);