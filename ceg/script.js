// script.js

// create the module and name it app
// also include ngRoute for all our routing needs
var app = angular.module('app', ['ngRoute', 'ngCookies', 'LocalStorageModule', 'ui.bootstrap', 'moment-picker']);
app.directive('repeatDone', function () {
    return {
        link: function (scope, elem, attrs) {
            $('#table_id').DataTable();
        }
    };
});

app.directive("w3TestDirective", function () {
    return {
        template: "<h1>Made by a directive!</h1>"
    };
});

// app.service('hexafy', function() {
//     this.myFunc = function (x) {
//         
//     }
// });


// configure our routes
app.config(function ($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'pages/index.html',
            controller: 'indexController'
        })
        .when('/ceg', {
            templateUrl: 'pages/index.html',
            controller: 'indexController'
        })

        // route for the home page
        .when('/demom', {
            templateUrl: 'pages/demom.html',
            controller: 'indexController'

        })

        .when('/aboutceg', {

            templateUrl: 'pages/aboutceg.html',
            controller: 'indexController',

            resolve: {

                "check": function ($location) {

                    if (window.sessionStorage.currentLoggedInUser != undefined) {
                        $location.path('/aboutceg');
                    } else if (window.sessionStorage.currentLoggedInUser === undefined) {
                        $location.path('/');
                    }
                }
            }
        })

        .when('/HealthFirtility', {
            templateUrl: 'pages/MenuPages/HealthFirtility.html',
            controller: 'indexController'
        })
        .when('/slides', {
            templateUrl: 'pages/slides.html'
        })


        .when('/BalancedBreeding', {
            templateUrl: 'pages/MenuPages/BalancedBreeding.html',
            controller: 'indexController'
        })

        .when('/FeedEfficiency', {
            templateUrl: 'pages/MenuPages/FeedEfficiency.html',
            controller: 'indexController'
        })

        .when('/A2', {
            templateUrl: 'pages/MenuPages/A2.html',
            controller: 'indexController'
        })

        .when('/Holstein', {
            templateUrl: 'pages/MenuPages/Holstein.html',
            controller: 'indexController'
        })

        .when('/about', {
            templateUrl: 'pages/MenuPages/about.html',
            controller: 'indexController'
        })

        .when('/careers', {
            templateUrl: 'pages/MenuPages/careers.html',
            controller: 'indexController'
        })

        .when('/news', {
            templateUrl: 'pages/MenuPages/news.html',
            controller: 'indexController'
        })

        .when('/contactus', {
            templateUrl: 'pages/MenuPages/contactus.html',
            controller: 'indexController'
        })







        .when('/kkk', {
            templateUrl: 'pages/kkk.html',
            controller: 'loginController'


        })


        // route for the about page
        .when('/aboutceg', {
            templateUrl: 'pages/aboutceg.html',
            controller: 'aboutController'
        })
        .when('/login', {
            templateUrl: 'pages/login.html',
            controller: 'loginController'
        })

        .otherwise({
            templateUrl: "pages/login.html"
        });


});
// app.filter('removeSpaces', [function() {
//     return function(string) {
//         if (!angular.isString(string)) {
//             return string;
//         }
//         return string.replace(/[\s]/g, '');
//     };
// }])